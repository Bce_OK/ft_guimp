# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: kmeera-r <kmeera-r@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/20 19:22:42 by hgreenfe          #+#    #+#              #
#    Updated: 2020/06/16 20:59:09 by hgreenfe         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# used variables

NAME = ft_guimp
LIBUI = ui
SRCDIR = srcs/
OBJDIR = objs/
LIBSDIR = libs/
LIBUIDIR = libui/
INCDIR1 = incs/
INCDIR2 = $(LIBSDIR)SDL/include
LIBDIR = $(LIBSDIR)libft
LIBS = ft SDL2

# used applications

CC = gcc
CCFLAGS = -g -Wall -Wextra -Werror -pedantic-errors -I $(INCDIR1) -I $(INCDIR2)
AR = ar
ARFLAGS = -rsc
RM = rm
RMFLAGS = -rf

#used directories and files
# LIBFILES := ft_events \
# 		ft_window \
# 		ft_window_params \
# 		libui_defaults \
# 		ft_control \
# 		ft_control_destructors \
# 		ft_drawable \
# 		redraw_functions \
#         ft_controlevent \
#         ft_libui \
#         ft_event_func

FILES := main \
		ft_guimp \
		ft_tools \
		tools/ft_colorbar \
		tools/ft_filler \
		tools/ft_pencil \
		tools/ft_brush

HEADERS = $(INCDIR1)/SDL2/SDL.h $(INCDIR1)/ft_libui.h

# LIBSRCS = $(addsuffix .c, $(LIBFILES))
# LIBOBJS = $(addsuffix .o, $(LIBFILES))
# FULL_LIBSRCS = $(addprefix $(SRCDIR)$(LIBUIDIR), $(LIBSRCS))
# FULL_LIBOBJS = $(addprefix $(OBJDIR)$(LIBUIDIR), $(LIBOBJS))

SRCS = $(addsuffix .c, $(FILES))
OBJS = $(addsuffix .o, $(FILES))
FULL_SRCS = $(addprefix $(SRCDIR), $(SRCS))
FULL_OBJS = $(addprefix $(OBJDIR), $(OBJS))
FULL_LIBS = $(addprefix -l, $(LIBS))
FRAMEWORK = -framework OpenGL -framework Cocoa
#  -framework iconv

ifeq ($(OS),Windows_NT)
	PWD := $(shell cd)
else
	PWD := $(shell pwd)
endif


PATH_SDL = $(addsuffix /libs/libui/SDL, $(PWD))
SDL =  $(PATH_SDL)/SDL2/build/.libs

LIBFLAGS = -L$(LIBDIR) -lft -L $(LIBSDIR)$(LIBUIDIR) -l$(LIBUI) -L $(SDL) -lSDL2

.PHONY:  all re clean fclean $(LIBDIR)

all: $(NAME) $(LIBUI)

$(OBJDIR):
	mkdir $(OBJDIR)
	mkdir $(OBJDIR)/tools

$(LIBDIR):
	make -C $(LIBDIR)

$(OBJDIR)%.o: $(SRCDIR)%.c
	$(CC) $(CCFLAGS) -c -o $@ $<


#$(SDL):
#	cd $(PATH_SDL)/SDL2; ./configure --prefix=$(PATH_SDL); make;
#	make -sC $(PATH_SDL)/SDL2 install
#

$(LIBUI):
	make -C $(LIBSDIR)$(LIBUIDIR)

# $(SDL)
$(NAME):  $(LIBDIR) $(LIBUI) $(OBJDIR) $(FULL_OBJS)
	$(CC) $(CCFLAGS) -o $(NAME) $(FRAMEWORK) $(LIBFLAGS) $(FULL_OBJS)

clean: clean_ui
	make -C $(LIBDIR) clean
	$(RM) $(RMFLAGS) $(FULL_OBJS)

clean_ui:
	make -C $(LIBSDIR)$(LIBUIDIR) clean
#	make -C $(PATH_SDL)/SDL2_image clean

fclean_ui:
	make -C $(LIBSDIR)$(LIBUIDIR) fclean

fclean: fclean_ui clean
	make -C $(LIBDIR) fclean
	$(RM) $(RMFLAGS) $(NAME)

re: fclean all

