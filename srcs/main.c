/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hgreenfe <hgreenfe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 22:28:24 by hgreenfe          #+#    #+#             */
/*   Updated: 2019/06/28 09:31:24 by hgreenfe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ftui.h"
#include <math.h>
#include "ft_guimp.h"

void	ft_set_colorbar_func(void *tool, void *param,
		t_controlevent_func color_func)
{
	set_controlevent_function(tool, FT_EVENT_RENDER,
			&on_redraw_tool1, NULL);
	set_controlevent_function(tool, FT_EVENT_MOUSEMOVE,
			color_func, param);
	set_controlevent_function(tool, FT_EVENT_MOUSEDOWN,
			&on_tools_set, param);
	set_controlevent_function(tool, FT_EVENT_MOUSEUP,
			&on_tools_unset, param);
}

void	ft_on_toolbrush_click(void *ctrl, int num, void *param)
{
	t_tool			*tool;

	(void)num;
	(void)param;
	tool = ft_get_control_param(ctrl);
	if (tool->type == TT_BRUSH)
	{
		tool->type = TT_PENCIL;
		ft_set_control_color(ctrl, 0xffff00);
	}
	else if (tool->type == TT_PENCIL)
	{
		tool->type = TT_FILLER;
		ft_set_control_color(ctrl, 0xff00);
	}
	else if (tool->type == TT_FILLER)
	{
		tool->type = TT_BRUSH;
		ft_set_control_color(ctrl, 0xff00ff);
	}
	ft_set_control_param(ctrl, tool);
}

void	ft_init_tools(void *wnd2, void *param)
{
	void		*tool_red;
	void		*tool_green;
	void		*tool_blue;
	void		*tool_alpha;
	void		*tool_size;
	void		*tool_color;
	void		*tool_brush;

	tool_red = ft_create_new_control(ft_new_rect(10, 100, 256, 50),
			0x00ff0000);
	tool_green = ft_create_new_control(ft_new_rect(10, 170, 256, 50),
			0x0000ff00);
	tool_blue = ft_create_new_control(ft_new_rect(10, 240, 256, 50),
			0x000000ff);
	tool_alpha = ft_create_new_control(ft_new_rect(10, 310, 256, 50),
			0xff000000);
	tool_size = ft_create_new_control(ft_new_rect(10, 370, 256, 50),
			ft_get_window_color(wnd2) >> 8u);
	tool_color = ft_create_new_control(ft_new_rect(10, 440, 50, 50), 0);
	tool_brush = ft_create_new_control(ft_new_rect(80, 440, 50, 50), 0xffff00);
	ft_set_colorbar_func(tool_red, param, &update_tools_red);
	ft_set_colorbar_func(tool_green, param, &update_tools_green);
	ft_set_colorbar_func(tool_blue, param, &update_tools_blue);
	ft_set_colorbar_func(tool_alpha, param, &update_tools_alpha);
	ft_set_colorbar_func(tool_size, param, &ft_tool_set_size);
	set_controlevent_function(tool_size, FT_EVENT_RENDER,
			&ft_tool_size_render, param);
	set_controlevent_function(tool_color, FT_EVENT_RENDER,
			&ft_tool_color_render, param);
	set_controlevent_function(tool_color, FT_EVENT_TICKFUNC,
			&ft_on_tick, param);
	set_controlevent_function(tool_brush, FT_EVENT_MOUSEUP,
			&ft_on_toolbrush_click, param);
	set_controlevent_function(tool_brush, FT_EVENT_RENDER,
			&on_redraw_tool1, param);
	ft_attach_to_window(tool_red, wnd2);
	ft_attach_to_window(tool_green, wnd2);
	ft_attach_to_window(tool_blue, wnd2);
	ft_attach_to_window(tool_alpha, wnd2);
	ft_attach_to_window(tool_size, wnd2);
	ft_attach_to_window(tool_color, wnd2);
	ft_attach_to_window(tool_brush, wnd2);
}

void	ft_guimp_init(t_list **list)
{
	t_rect		w_r;

	if (!list)
		return ;
	w_r.x = 200;
	w_r.y = 0xFFFF0000u;
	w_r.w = 300;
	w_r.h = 768;
	add_window((void**)list, w_r, 0x909090ff, "test1");
	w_r.x = 500;
	w_r.w = 1024;
	w_r.h = 768;
	add_window((void**)list, w_r, 0xffffffff, "test2");
}

void	ft_on_filler_up(void *ctrl, int num, void *event)
{
	t_point	pos;
	t_tool	*tool;

	pos = ft_get_mouse_event_state(event, &num);
	tool = ft_get_control_param(ctrl);
	if (tool &&
	tool->type == TT_FILLER)
		ft_fill(ctrl, pos, tool->color, tool->size);
}

void	ft_create_layouts(void *wnd, void *param)
{
	void	*layout1;
	t_point	size;
	t_rect	rect;

	size = ft_get_window_size(wnd);
	rect = ft_new_rect(0, 0, size.x, size.y);
	layout1 = ft_create_new_control(rect, 0xffffffffu);
	set_controlevent_function(layout1,
		FT_EVENT_MOUSEMOVE, &on_mouse_move, param);
	set_controlevent_function(layout1,
		FT_EVENT_MOUSEUP, &ft_on_filler_up, param);
	ft_attach_to_window(layout1, wnd);
}

int		main(int argc, char **argv)
{
	t_list		*list;
	t_tool		*tool;

	(void)argc;
	(void)argv;
	tool = ft_memalloc(sizeof(t_tool));
	tool->size = 0;
	list = ft_libui_init();
	ft_guimp_init(&list);
	set_event_function(list->content, FT_EVENT_KEYPRESS, &on_key_press, tool);
	set_event_function(list->content, FT_EVENT_MOUSEDOWN, &on_mouse_down, tool);
	set_event_function(list->content, FT_EVENT_MOUSEUP, &on_mouse_up, tool);
	set_event_function(list->next->content, FT_EVENT_KEYPRESS,
			&on_key_press, tool);
	ft_init_tools(list->next->content, tool);
	ft_create_layouts(list->content, tool);
	ft_mainloop(list);
	ft_libui_quit(list);
	ft_memdel((void**)&tool);
	return (0);
}
