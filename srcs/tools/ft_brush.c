/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_brush.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hgreenfe <hgreenfe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/20 15:45:16 by hgreenfe          #+#    #+#             */
/*   Updated: 2019/06/22 20:44:17 by hgreenfe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftui.h"
#include "ft_guimp.h"

void	ft_tool_size_render(void *tool1, int pitch, void *pixels)
{
	int				i;
	int				size;
	t_rect			rect;
	unsigned int	bg_color;

	(void)pitch;
	i = 0;
	rect = ft_get_control_rect(tool1);
	bg_color = ft_get_control_color(tool1);
	while (i < rect.w * rect.h)
	{
		size = (i % rect.w) * rect.h / rect.w + 1;
		if (i / rect.w <= size)
			(((unsigned int *)pixels)[i]) = 0xff000000;
		else
			(((unsigned int *)pixels)[i]) = bg_color;
		++i;
	}
}

void	ft_tool_set_size(void *ctrl, int btn, void *e)
{
	t_tool	*tool;
	t_rect	rect;

	tool = ft_get_control_param(ctrl);
	rect = ft_get_control_rect(ctrl);
	if (btn > 0)
		if ((tool->state & (1u << (unsigned)FTUI_MOUSE_LEFT)) > 0)
		{
			tool->size = update_tools(ctrl, e) * rect.h / rect.w;
		}
}

void	ft_on_tick(void *ctrl, int pitch, void *param)
{
	t_rect	rect;
	t_tool	*tool;
	t_rect	cur;
	t_point	c_size;
	void	*pixels;

	(void)pitch;
	(void)param;
	rect = ft_get_control_rect(ctrl);
	tool = ft_get_control_param(ctrl);
	if (tool)
	{
		rect.w = tool->size;
		rect.h = tool->size;
		ft_set_control_rect(ctrl, rect);
		pixels = ft_memalloc(sizeof(int) * tool->size * tool->size);
		rect = ft_new_rect(0, 0, tool->size, tool->size);
		cur = ft_new_rect(
				tool->size / 2, tool->size / 2, tool->size, tool->size);
		c_size = ft_new_point(tool->size, tool->size);
		ft_draw_circle(pixels, &rect, &cur, tool->color);
		ft_set_cursor(pixels, c_size,
				ft_new_point(tool->size / 2, tool->size / 2));
		ft_memdel(&pixels);
	}
}
