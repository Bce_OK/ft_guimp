/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pencil.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hgreenfe <hgreenfe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/03 21:27:38 by hgreenfe          #+#    #+#             */
/*   Updated: 2019/06/28 09:31:29 by hgreenfe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_guimp.h"
#include "libft.h"
#include "ftui.h"
#include <math.h>
#include "SDL2/SDL.h"

void	on_key_press(void *wnd, int keycode, void *event)
{
	(void)event;
	if (keycode == FTUI_KEY_ESCAPE)
		ft_set_window_quit(wnd, 1);
}

void	on_mouse_down(void *wnd, int mouse, void *event)
{
	t_point	m;
	int		state;
	t_tool	*tool;

	m = ft_get_mouse_event_state(event, &state);
	if (mouse == FTUI_MOUSE_LEFT)
	{
		tool = ft_get_window_input_param(wnd);
		tool->x = m.x;
		tool->y = m.y;
	}
}

void	on_mouse_up(void *wnd, int mouse, void *event)
{
	t_tool	*tool;
	t_point	*p;

	p = ft_memalloc(sizeof(t_point) * 2);
	tool = ft_get_window_input_param(wnd);
	if (mouse == FTUI_MOUSE_LEFT)
	{
		p[1] = ft_get_mouse_event_state(event, NULL);
		p[0].x = tool->x;
		p[0].y = tool->y;
		if (tool->type != TT_FILLER)
			draw_line(wnd, p, tool->color, tool->size);
	}
}

void	draw_func(void *ctrl, t_tool *tool, t_point *p)
{
	if (tool->type == TT_BRUSH)
		draw_line(ctrl, p, tool->color, tool->size);
	else if (tool->type == TT_PENCIL)
		draw_line(ctrl, p, tool->color, 2);
}

void	on_mouse_move(void *ctrl, int mouse, void *event)
{
	t_tool	*tool;
	t_point	*p;

	p = ft_memalloc(sizeof(t_point) * 2);
	p[1] = ft_get_mouse_event_state(event, NULL);
	tool = ft_get_control_param(ctrl);
	if (mouse == FTUI_MOUSE_LEFT)
	{
		p[0].x = tool->x;
		p[0].y = tool->y;
		draw_func(ctrl, tool, p);
	}
	tool->x = p[1].x;
	tool->y = p[1].y;
}
