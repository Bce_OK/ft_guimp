/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_colorbar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hgreenfe <hgreenfe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/20 13:25:58 by hgreenfe          #+#    #+#             */
/*   Updated: 2019/06/20 17:27:37 by hgreenfe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftui.h"
#include "ft_guimp.h"

void			ft_tool_color_render(void *tool1, int pitch, void *pixels)
{
	t_rect			rect;
	t_tool			*tool;
	int				i;

	(void)pitch;
	rect = ft_get_control_rect(tool1);
	tool = ft_get_control_param(tool1);
	if (tool)
	{
		i = 0;
		while (i < rect.w * rect.h)
		{
			(((unsigned int*)pixels)[i]) = tool->color;
			++i;
		}
	}
}

unsigned int	ft_get_result_color(unsigned int color, int bright,
									unsigned int mask)
{
	bright = (unsigned)bright | (unsigned)bright << 8u
			| (unsigned)bright << 16u | (unsigned)bright << 24u;
	return ((color & ~mask) | ((unsigned)bright & mask));
}
