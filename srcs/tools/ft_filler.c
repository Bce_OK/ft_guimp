/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_filler.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hgreenfe <hgreenfe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/23 03:26:30 by hgreenfe          #+#    #+#             */
/*   Updated: 2019/07/07 17:45:31 by hgreenfe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "ftui.h"

int		ft_is_diff(unsigned int old_color, unsigned int new_color, int diff)
{
	unsigned char	comp;
	unsigned char	shift;
	unsigned int	mask;

	shift = 0;
	mask = 0xffu;
	if (old_color == new_color)
		return (0);
	while (shift < 32)
	{
		comp = (((old_color & (mask << shift)) -
				(new_color & (mask << shift)))
				>> shift);
		if ((comp > diff || comp < -diff))
			return (1);
		shift += 8;
	}
	return (0);
}

int		ft_p_in_r(t_point p, t_rect r)
{
	return (p.x > 0 && p.x < r.w && p.y > 0 && p.y < r.h);
}

int		ft_set_color(unsigned int *pixels, t_point p, t_rect r,
		unsigned int color)
{
	if (ft_p_in_r(p, r) && pixels[p.x + p.y * r.w] != color)
	{
		pixels[p.x + p.y * r.w] = color;
		return (1);
	}
	else
		return (0);
}

t_list	*ft_push_pixel(void *ctrl, t_point p, unsigned int c,
		unsigned int new_c)
{
	t_list				*queue;
	unsigned int *const	pxls = ft_get_control_pixels(ctrl);
	t_rect const		rect = ft_get_control_rect(ctrl);

	(void)p;
	(void)c;
	(void)new_c;
	(void)pxls;
	(void)rect;
	queue = NULL;
	return (queue);
}

void	ft_fill(void *ctrl, t_point pnt, unsigned int color, int disp)
{
	const t_rect		rect = ft_get_control_rect(ctrl);
	unsigned int *const	pxls = ft_get_control_pixels(ctrl);
	unsigned int		old_c;
	t_list				*queue;

	old_c = pxls[pnt.x + pnt.y * rect.w];
	queue = NULL;
	(void)disp;
	ft_set_color(pxls, pnt, rect, color);
}
