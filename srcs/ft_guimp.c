/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_guimp.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hgreenfe <hgreenfe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/02 18:58:01 by hgreenfe          #+#    #+#             */
/*   Updated: 2019/06/22 21:47:24 by hgreenfe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftui.h"
#include "ft_guimp.h"

unsigned int	update_tools(void *ctrl, void *e)
{
	t_rect			rect;
	t_point			mouse;
	int				state;

	rect = ft_get_control_rect(ctrl);
	mouse = ft_get_mouse_event_state(e, &state);
	return (ft_pos(&rect, mouse.x, mouse.y));
}

void			update_tools_red(void *ctrl, int bright, void *e)
{
	t_tool			*tool;

	tool = ft_get_control_param(ctrl);
	if ((tool->state & (1u << (unsigned)FTUI_MOUSE_LEFT)) > 0)
	{
		bright = update_tools(ctrl, e);
		tool->color = ft_get_result_color(tool->color, bright, 0x00ff0000u);
	}
}

void			update_tools_green(void *ctrl, int bright, void *e)
{
	t_tool			*tool;

	tool = ft_get_control_param(ctrl);
	if ((tool->state & (1u << (unsigned)FTUI_MOUSE_LEFT)) > 0)
	{
		bright = update_tools(ctrl, e);
		tool->color = ft_get_result_color(tool->color, bright, 0x0000ff00u);
	}
}

void			update_tools_blue(void *ctrl, int bright, void *e)
{
	t_tool			*tool;

	tool = ft_get_control_param(ctrl);
	if ((tool->state & (1u << (unsigned)FTUI_MOUSE_LEFT)) > 0)
	{
		bright = update_tools(ctrl, e);
		tool->color = ft_get_result_color(tool->color, bright, 0x000000ffu);
	}
}

void			update_tools_alpha(void *ctrl, int bright, void *e)
{
	t_tool			*tool;

	tool = ft_get_control_param(ctrl);
	if ((tool->state & (1u << (unsigned)FTUI_MOUSE_LEFT)) > 0)
	{
		bright = update_tools(ctrl, e);
		tool->color = ft_get_result_color(tool->color, bright, 0xff000000u);
	}
}
