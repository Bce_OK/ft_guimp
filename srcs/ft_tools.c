/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tools.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hgreenfe <hgreenfe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/04 21:39:29 by hgreenfe          #+#    #+#             */
/*   Updated: 2019/06/19 21:42:17 by hgreenfe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_guimp.h"
#include "libft.h"
#include "ftui.h"

#define MAX_COLOR_BRIGHT  255

void			on_redraw_tool1(void *tool1, int pitch, void *pixels)
{
	int				i;
	unsigned int	col;
	t_rect			rect;
	unsigned int	bg_color;
	t_tool			*t;

	i = 0;
	rect = ft_get_control_rect(tool1);
	bg_color = ft_get_control_color(tool1);
	t = ft_get_control_param(tool1);
	while (i < pitch * rect.h / 4)
	{
		col = (i % rect.w) * MAX_COLOR_BRIGHT / rect.w;
		col = (col << 24u) | (col << 16u) | (col << 8u) | (col);
		if ((t->color & bg_color) != (col & bg_color))
			(((unsigned int *)pixels)[i]) = bg_color & col;
		else
			(((unsigned int *)pixels)[i]) = 0xffffffffu;
		++i;
	}
}

void			on_tools_set(void *ctrl, int btn, void *e)
{
	t_tool		*tool;

	(void)e;
	tool = ft_get_control_param(ctrl);
	tool->state |= (1u << (unsigned)btn);
}

void			on_tools_unset(void *ctrl, int btn, void *e)
{
	t_tool		*tool;

	(void)e;
	tool = ft_get_control_param(ctrl);
	tool->state &= (0xffffffffu ^ (1u << (unsigned)btn));
}

int				ft_pos(t_rect *rect, int x, int y)
{
	if (x >= rect->x && x <= rect->x + rect->w
		&& y >= rect->y && y <= rect->y + rect->h)
	{
		return (x - rect->x);
	}
	else
		return (-1);
}
