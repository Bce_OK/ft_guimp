/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_guimp.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hgreenfe <hgreenfe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 22:57:59 by hgreenfe          #+#    #+#             */
/*   Updated: 2019/06/23 03:40:33 by hgreenfe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_GUIMP_H
# define FT_GUIMP_H

# include "libft.h"
# include "ftui.h"

typedef enum		e_tooltype {
	TT_PENCIL,
	TT_BRUSH,
	TT_FILLER,
	TT_RECT,
	TT_CIRCLE
}					t_tooltype;

typedef	struct		s_tool {
	t_tooltype		type;
	int				x;
	int				y;
	int				size;
	unsigned int	color;
	unsigned int	state;
}					t_tool;

typedef	void		(*t_func)(t_list *);
typedef	void		(*t_func2)(t_list *, void* param);

int					ft_process(t_list *list);
void				ft_libui_quit(t_list *list);
void				ft_window_destroy(void *wnd, size_t size);
void				on_key_press(void *wnd, int keycode, void *event);
void				on_redraw(void *wnd, int num, void *param);
void				on_mouse_down(void *wnd, int mouse, void *event);
void				on_mouse_up(void *wnd, int mouse, void *event);
void				on_mouse_move(void *wnd, int mouse, void *event);
void				on_tools_set(void *wnd, int btn, void *e);
void				on_tools_unset(void *wnd, int btn, void *e);
unsigned int		update_tools(void *ctrl, void *e);
void				update_tools_red(void *wnd, int bright, void *e);
void				update_tools_green(void *wnd, int bright, void *e);
void				update_tools_blue(void *wnd, int bright, void *e);
void				update_tools_alpha(void *wnd, int bright, void *e);
void				on_redraw_tool1(void *tool1, int pintch, void *pixels);
int					ft_pos(t_rect *rect, int x, int y);
unsigned int		ft_get_result_color(unsigned int color, int bright,
										unsigned int mask);
void				ft_tool_color_render(void *tool1, int pitch, void *pixels);
void				ft_tool_size_render(void *tool1, int pitch, void *pixels);
void				ft_tool_set_size(void *ctrl, int btn, void *e);
void				ft_on_tick(void *ctrl, int pitch, void *param);
void				ft_fill(void *ctrl, t_point pnt,
							unsigned int color, int disp);
#endif
